# Используем JRE для сборки
FROM openjdk:11.0.11-jre-slim-buster

# Копируем скрипт запуска
COPY start-spark.sh /

# Устанавливаем команду запуска
CMD ["/bin/bash", "/start-spark.sh"]

# Устанавливаем Spark
ENV SPARK_VERSION=3.0.2
ENV HADOOP_VERSION=3.2
ENV SPARK_HOME=/opt/spark
ENV PYTHONHASHSEED=1

# Настраиваем переменные окружения
ENV SPARK_MASTER_PORT=7077
ENV SPARK_MASTER_WEBUI_PORT=8080
ENV SPARK_LOG_DIR=/opt/spark/logs
ENV SPARK_MASTER_LOG=/opt/spark/logs/spark-master.out
ENV SPARK_WORKER_LOG=/opt/spark/logs/spark-worker.out
ENV SPARK_WORKER_WEBUI_PORT=8080
ENV SPARK_WORKER_PORT=7000
ENV SPARK_MASTER="spark://spark-master:7077"
ENV SPARK_WORKLOAD="master"

# Открываем порты
EXPOSE 8080 7077 7000

WORKDIR /opt/spark

# Устанавливаем зависимости для PySpark
RUN apt-get update
RUN apt-get install -y curl
RUN apt-get install -y vim
RUN apt-get install -y wget
RUN apt-get install -y software-properties-common
RUN apt-get install -y ssh
RUN apt-get install -y net-tools
RUN apt-get install -y ca-certificates
RUN apt-get install -y python3
RUN apt-get install -y python3-pip
RUN apt-get install -y python3-numpy
RUN apt-get install -y python3-matplotlib
RUN apt-get install -y python3-scipy
RUN apt-get install -y python3-pandas
RUN apt-get install -y python3-simpy
RUN update-alternatives --install "/usr/bin/python" "python" "$(which python3)" 1

RUN wget --no-verbose -O apache-spark.tgz "https://archive.apache.org/dist/spark/spark-${SPARK_VERSION}/spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz"
RUN mkdir -p /opt/spark
RUN tar -xf apache-spark.tgz -C /opt/spark --strip-components=1
RUN rm apache-spark.tgz


# Настраиваем Spark логи
RUN mkdir -p $SPARK_LOG_DIR
RUN touch $SPARK_MASTER_LOG
RUN touch $SPARK_WORKER_LOG
RUN ln -sf /dev/stdout $SPARK_MASTER_LOG
RUN ln -sf /dev/stdout $SPARK_WORKER_LOG


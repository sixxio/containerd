#!/bin/bash

kubectl apply -f spark-configmap.yaml
kubectl apply -f spark-pv.yaml --force
kubectl apply -f spark-master-deployment.yaml --force
kubectl apply -f spark-worker-deployment.yaml --force
kubectl apply -f spark-master-service.yaml --force

# kubectl get pods | awk '/spark/ {print $1}' | xargs -I {} kubectl logs {}
# kubectl get pods | awk '/spark/ {print $1}' | xargs -I {} kubectl delete pods {}
kubectl get pods

import os
import subprocess

directories = [
    # "./apps",
    # "./data",
    "/opt/spark-data",
    "/opt/spark-apps",
]

for directory in directories:
    if not os.path.exists(directory):
        os.makedirs(directory)
        print(f"Directory {directory} created.")
    else:
        print(f"Directory {directory} already exists.")

def copy_file(src, dst):
    """Копирует файл из src в dst."""
    try:
        subprocess.run(['cp', src, dst], check=True)
        print("Файл успешно скопирован.")
    except subprocess.CalledProcessError as e:
        print(f"Ошибка: команда cp завершилась с кодом {e.returncode}.")
    except FileNotFoundError as e:
        print(f"Ошибка: файл '{src}' не найден.")
    except PermissionError as e:
        print(f"Ошибка: у вас недостаточно прав для копирования файла '{src}'.")
    except OSError as e:
        print(f"Ошибка: возникла ошибка ОС при выполнении команды cp.")

for file in os.listdir("./scripts_for_spark"):
    copy_file(file, '/opt/spark-apps/')

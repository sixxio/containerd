#!/bin/bash

set -e

kubectl apply -f postgres/pg_configmap.yaml
kubectl apply -f postgres/pg_secret.yaml
kubectl apply -f postgres/pg_service.yaml
kubectl apply -f postgres/pg_deployment.yaml

kubectl apply -f nextcloud/nextcloud_configmap.yaml
kubectl apply -f nextcloud/nextcloud_secret.yaml
kubectl apply -f nextcloud/nextcloud_deployment.yaml

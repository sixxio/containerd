import os

directories = [
    "./apps",
    "./data",
]

for directory in directories:
    if not os.path.exists(directory):
        os.makedirs(directory)
        print(f"Directory {directory} created.")
    else:
        print(f"Directory {directory} already exists.")
